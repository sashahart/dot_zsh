# Do warn me if I'm being dumb.
setopt BAD_PATTERN
# zsh editor ignores ^S/^Q
unsetopt FLOW_CONTROl
# allow comments in interactive session, in case I'm recording or something - why not?
setopt INTERACTIVE_COMMENTS
# I shouldn't work late
setopt RM_STAR_WAIT
# Allow Ctrl-S to pass through
setopt NO_FLOW_CONTROL
# share history file across instances
setopt SHARE_HISTORY
# in history searches from the line editor, just show each command once
setopt HIST_FIND_NO_DUPS
# don't add consecutive dups to history
setopt HIST_IGNORE_DUPS
# don't put stuff prefixed with space in history
setopt HIST_IGNORE_SPACE
# don't store use of history cmd
setopt HIST_NO_STORE
# store timing and duration of commands
setopt EXTENDED_HISTORY
# really big history.
HISTSIZE=10000 && SAVEHIST=$HISTSIZE
# Idiosyncratic place to put history file.
HISTFILE=~/.history
# Spammy commands not to include in history at all.
HISTIGNORE="&:ls:[bf]g:exit:reset:clear"
# I usually need this most when I didn't think to use pushd
setopt AUTO_PUSHD
# I find it error prone to just change to named directory
unsetopt autocd 
# I don't really understand zsh use of characters like 
# (even if ** is cool). Maybe later...
unsetopt EXTENDED_GLOB
