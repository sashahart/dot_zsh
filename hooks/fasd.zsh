fasd_cache="$HOME/.fasd-init-cache"
orig_a=$(alias a)
if [[ "${commands[fasd]}" -nt "$fasd_cache" || ! -s "$fasd_cache"  ]]; then
	init_args=(posix-alias zsh-hook zsh-ccomp zsh-ccomp-install zsh-wcomp zsh-wcomp-install)
	fasd --init "$init_args[@]" >! "$fasd_cache" 2> /dev/null
fi
source "$fasd_cache"
alias a > /dev/null
if [ $? -eq 0 ]; then
	unalias a
fi
alias "$orig_a"
unset fasd_cache init_args
