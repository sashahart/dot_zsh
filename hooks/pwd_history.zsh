# directory history scroller by Sasha.
# 
# Suppose I start in /1 then go to /2, /3, and /4 in sequence.
# zsh directory stack looks like this:
#	/4 /3 /2 /1
# and $PWD is /4 (in other words, it's at index 1).
# Now suppose I popd to go backward, I get
#	/3 /2 /1
# and $PWD is /3 (in other words, it's still at index 1).
# that is 'backwards' in history. But suppose now that I
# want to go 'forward', reversing that backwards motion.
# I can just cd /4 and restore the previous state.
# But that /4 has to be stored somewhere to do this.
# INSTEAD OF popd, you can use pushd +1 to remember /4
#	/3 /2 /1 /4
# Then to access it you have to pushd to /4, which
# you can do with `pushd -0`.
# Now a complication arises. If you are at /4 /3 /2 /1
# and you issue pushd /5, everything is still simple.
# But if you back up to /1 (/1 /4 /3 /2) and issue pushd /a,
# you will get (/a /1 /4 /3 /2); from there,forward gives (/2 /a /1 /4 /3) and
# backward gives (/1 /4 /3 /2 /a). Since putting all directories on a tape is
# not so useful, it might be nice to truncate the 'forward' history
# (/1 /4 /3 /2 -> /a /1). To clear out everything but /1, 
#   to_save=$(( ( `dirs | wc -w` - $dir_index ) + 1 ))
#   pushd +$to_save
#	repeat $(( $dir_index - 1 )) popd 
#	cd /a
# - The wrapping around can be a little confusing, so $dir_index is used to
#   'stop' at both ends of the stack
# - Spamming of `dirs` is suppressed with setopt pushd_silent
setopt pushd_silent
dir_index=1
function directory-back () {
	if [[ $dir_index -lt `dirs | wc -w` ]]; then
		pushd +1 >/dev/null
		dir_index=$(( $dir_index + 1 ))
		zle reset-prompt
	fi
}
function directory-forward () {
	if [[ $dir_index -gt 1 ]]; then
		pushd -0 >/dev/null
		dir_index=$(( $dir_index - 1 ))
		zle reset-prompt
	fi
}
# Yeah, replacing/wrapping cd is kind of lame,
# and doesn't catch autocd case.
function cd () {
 	if builtin cd $@; then
		if [[ $dir_index -gt 1 ]]; then
			# Clear out everything after $dir_index in stack
			to_save=$(( ( `dirs | wc -w` - $dir_index ) + 1 ))
			pushd +$to_save
			repeat $(( $dir_index - 1 )) popd 
		fi
	fi
	dir_index=1
}
# Like back, but cd ..
function directory-up () {
	if [[ "$PWD" != "/" ]]; then
		pushd ..
		zle reset-prompt
	fi
}
zle -N directory-back 
zle -N directory-forward
zle -N directory-up
bindkey "\e[1;5D" directory-back
bindkey "\e[1;5C" directory-forward
bindkey "\e[1;2D" directory-up
bindkey "\e[1;2C" directory-back

