autoload -Uz vcs_info
zstyle ':vcs_info:*' disable cdv cvs darcs fossil mtn p4 svk tla
vcs_info_update () {
  psvar=()
  vcs_info
  [[ -n $vcs_info_msg_0_ ]] && psvar[1]="$vcs_info_msg_0_"
}
add-zsh-hook precmd vcs_info_update

# not sure yet
zstyle ':vcs_info:git*:*' get-revision true
# allow %c %u - check for staged/unstaged
zstyle ':vcs_info:*' check-for-changes true
# for hash: add %12.12i
zstyle ':vcs_info:git:*' formats "%{$fg[black]%}± %{$fg[blue]%}%b%{$reset_color%} %m%c%u%{$reset_color%} "
zstyle ':vcs_info:git:*' actionformats '± %b|%a'
zstyle ':vcs_info:hg:*' formats "%{$fg[black]%}☿ %{$fg[blue]%}%b%{$reset_color%}"
zstyle ':vcs_info:hg:*' actionformats "☿ %b|%a"

#zstyle ':vcs_info:git*+set-message:*' hooks git-st # git-stash
# Show count of stashed changes
function +vi-git-stash() {
    local -a stashes

    if [[ -s ${hook_com[base]}/.git/refs/stash ]] ; then
        stashes=$(git stash list 2>/dev/null | wc -l)
        hook_com[misc]+=" (${stashes} stashed)"
    fi
}
# Show remote ref name and number of commits ahead-of or behind
function +vi-git-st() {
    local ahead behind remote
    local -a gitstatus

    # Are we on a remote-tracking branch?
    remote=${$(git rev-parse --verify ${hook_com[branch]}@{upstream} \
        --symbolic-full-name 2>/dev/null)/refs\/remotes\/}

    if [[ -n ${remote} ]] ; then
        # for git prior to 1.7
        # ahead=$(git rev-list origin/${hook_com[branch]}..HEAD | wc -l)
        ahead=$(git rev-list ${hook_com[branch]}@{upstream}..HEAD 2>/dev/null | wc -l)
        (( $ahead )) && gitstatus+=( "${c3}+${ahead}${c2}" )

        # for git prior to 1.7
        # behind=$(git rev-list HEAD..origin/${hook_com[branch]} | wc -l)
        behind=$(git rev-list HEAD..${hook_com[branch]}@{upstream} 2>/dev/null | wc -l)
        (( $behind )) && gitstatus+=( "${c4}-${behind}${c2}" )

        hook_com[branch]="${hook_com[branch]} [${remote} ${(j:/:)gitstatus}]"
    fi
}
