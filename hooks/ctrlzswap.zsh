# Quickly flip between zsh and a suspended console program: Ctrl-Z to a program
# suspends it to zsh,  Ctrl-Z to zsh swaps back to the program.
foreground() {
  %%
  zle redisplay
}
zle -N foreground
bindkey '^Z' foreground
