# $ZDOTDIR/.zshenv
# Sourced before the rest of .z* for all shells (unless -f is set).
# that includes scripts, remote ssh commands (non-interactive)...
# this tends to override other things and should be minimal

# Try to skip issuing compinit until one time in zshrc
skip_global_compinit=1

if [ -n "$DESKTOP_SESSION" ];then
    eval $(gnome-keyring-daemon --start --components=ssh)
    export SSH_AUTH_SOCK
fi
