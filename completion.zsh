# Reference
# ---------
# :completion:function:completer:command:argument:tag
#	function is a named widget calling the completion
# 	completer is the name of the active completion function w/o underscore
# 	command is the command (sometimes special, like cvs-add for 'cvs add')
# 	argument is a command line or option argument like argument-1 or option-opt-1
# 	(where opt is some option)
# 	tag is used to discriminate types of matches
#	- files directories local-directories manuals commands hosts jobs processes
# 	  signals tags urls groups users packages devices displays
# 	  names? builtins bookmarks options

# Used for development of this script - give current context, etc.
bindkey ^K _complete_help

# don't search old compctl system, I don't use it
zstyle ':completion:*' use-compctl false

# allow caching for very slow completions like dpkg/apt
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache

# completely case insensitive completion, for complete laziness
# (I don't hit tab if I mean to be precise, only when I mean to be fast)
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'

if [ -n "$LS_COLORS" ]; then 
	# Inherit colors from customization of GNU ls
	# this MUST run AFTER LS_COLORS is populated, e.g. by dircolors
	zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
else
	# Just use default colors
	zstyle ':completion:*:default' list-colors ''
fi

# At the first tab press, I want either completion (if unambiguous) OR display
# of possible completions with any common prefix filled in. If LIST_AMBIGUOUS
# is set, listing will NOT occur if there is a common prefix to insert, thus it
# only happens after two presses (and menu selection on the third press). If it
# is NOT set, then listing occurs for any ambiguity at the first press, and
# menu selection can occur at the second.
unsetopt list_ambiguous
setopt auto_list  # default: list choices when there is ambiguity.
#unsetopt auto_list # don't list choices when there is ambiguity - to avoid the list multipager...
setopt auto_menu  # default: menu complete on second press (but see about 'yes'). 
#setopt menucomplete # jump right into menu on first press, by default. overrides auto_menu
# suppress message asking 'do you wish to see all ... possibilities (... lines)?'
# (so second press can just pass right through to menu selection, without unsetopt auto_list)
LISTMAX=9998

# Instead of just cycling through completions inline, show them in a menu
# (second tab press) by default. add 'yes' to start in menu select;
# without 'yes' it takes two tab presses to get the first full completion,
# allowing the common prefix to be used after the first press.
# that wouldn't be necessary if it acted like incremental search (vim /)
# with 'yes' you can really only select from available options or backspace
# over the first completion text...
zstyle ':completion:*' menu select=0 # interactive # search

# Enable scrolling in completion lists - can just use '' to enable with default message.
#zstyle ':completion:*:default' list-prompt '%S(%p) Hit TAB for next page, or a character to insert%s'

# Enable scrolling in menu selection
zstyle ':completion:*:default' select-prompt '%S(%p) use cursor keys to scroll%s'

# Don't allow certain patterns to occur in _files matches - namely, backup
# files and ignoreable object/bytecode files.
# (however, these may still occur in default matches which run after _files,
# this just ensures they don't show up if there are other completions)
zstyle ':completion:*:(all-|)files' ignored-patterns \
	'*~' '*.(old|bak|swp|swo)' \
	'*.(pyc|pyo|elc|zwc)' '*.(o|lo)'

# sort filenames by last inode change time (kind of 'most recently used').
zstyle ':completion:*' file-sort change

# Ignore completion functions for commands you don’t have (http://grml.org/zsh/zsh-lovers.html):
zstyle ':completion:*:functions' ignored-patterns '_*'

# allow completion on foo// to show everything under foo including subdirs
zstyle ':completion:*' squeeze-slashes false

# When completing after ../ don't put in parents of the entered path
# and don't put in the current directory
zstyle ':completion:*:cd:*' ignore-parents parent pwd

# Default to quick incremental search on man pages
zstyle ':completion:*:manuals' menu yes interactive
zstyle ':completion:*:manuals' command apropos

# process tree menu for kill completion.
# if the current call stack includes the completer for sudo, then don't make it
# specific to user. otherwise, do make it specific to user, to pull in stuff
# from other terminals. need -e to do this kind of test at all.
zstyle -e ':completion:*:processes' command \
  'if (( $funcstack[(eI)$_comps[sudo]] )); then reply="ps -e --forest -o pid,user,cmd"; else reply="ps --forest -u $USER -o pid,cmd"; fi'
# first equal sign is the start of a zsh regex.
# (#b) enables backreferences
# ' #' is any amount of leading space (# is equivalent to normal regex *)
# '([0-9]#)' is a group with any number of digits in it
# the second = in the whole expression is the default color, subsequent ones define colors for each backref
# warning: need to load 'colors' before $color[x] will work, can also hardcode values instead.
# this gets more complicated because username is included in the readout for
# sudo kill, but not plain kill.
darkgray="$color[bold];$color[black]"
zstyle ':completion:*:*:kill:*:processes' list-colors "=(#b) #([0-9]#) #([A-Za-z][A-Za-z0-9\-_.]#)# #([\|\\_ ]# )([^ ]#)*=$color[blue]=$color[red]=$color[yellow]=$darkgray=$color[white]"
# simpler:
#   zstyle ':completion:*:processes' command 'ps -ax'
#   zstyle ':completion:*:*:kill:*:processes' command 'ps --forest -u $USER -o pid,cmd'

# drop right into interactive menu for killall completion
zstyle -e ':completion:*:processes-names' command \
  'if (( $funcstack[(eI)$_comps[sudo]] )); then reply="ps -e -o cmd"; else reply="ps -u $USER -o cmd"; fi'
zstyle ':completion:*:*:killall:*' menu yes select interactive
# simpler:
#   zstyle ':completion:*:killall:*' command 'ps -u $USER -o cmd' 
#   zstyle ':completion:*:processes-names' command 'ps -aeo comm='

# don't offer current shell process as a completion unless given unambiguously.
zstyle ':completion:*:processes' ignored-patterns " #$$"

# if file already mentioned in rm/kill/diff line, don't suggest it again
zstyle ':completion:*:(rm|kill|diff):*' ignore-line yes

# make user completion ignore some less interesting usernames
zstyle ':completion:*:*:*:users' ignored-patterns \
	abrt avahi avahi-autoipd bin bind chrony colord daemon dbus dictd ftp games gnats \
	gdm gopher halt ident identd junkbust lp mail mailnull man messagebus named news \
	nfsnobody nm-openconnect ntp openvpn operator pulse qemu qpidd radvd rpc rpcuser rpcusers \
	rpm rtkit saslauth shutdown smmsp smolt sshd sync tcpdump usbmuxd uucp xfs
