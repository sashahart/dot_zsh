# Fancy two line prompt with user@host on right top and directory/vcs info on
# left top. bright jobs indicator when there are jobs.

# Old one-line prompt
# export PROMPT="%{$fg[white]%}%{$bg[grey]%}%n@%m:%/%# %{$reset_color%}%{$fg[red]%}▎%{$reset_color%}"

# let prompt use variable substitution, turn off if not used...
setopt prompt_subst
# use color arrays for readability
autoload -Uz colors && colors

case $HOST in
    rex)
        hostcolor=green
        ;;
    hawk)
        hostcolor=blue
        ;;
    qand)
        hostcolor=magenta
        ;;
    *)
        hostcolor=gray
        ;;
esac

case $USER in
    root)
        usercolor=red
        ;;
    *)
        usercolor=gray
        ;;
esac

# load and configure vcs_info. add-zsh-hook is already loaded.
autoload -Uz vcs_info
# only need this for vcs I actually interact with
zstyle ':vcs_info:*' enable git hg svn
# allow %c %u - check for staged/unstaged
zstyle ':vcs_info:*' check-for-changes true
# lifted code for counting string width correctly with color
# from some adam prompt or something like that
prompt_width () {
	echo $(( ${#${(S%%)1//(\%([KF1]|)\{*\}|\%[Bbkf])}} ))
}
COLUMNS=$(tput cols)
vcs_info_update () {
	vcs_info
	#jobstring="%(1j.%j job%(2j.s.) .)"
	local jobcount=$#jobstates
	local jobstring=""
	if [[ $jobcount -eq 1 ]]; then
		jobstring="(1 job) "
	elif [[ $jobcount -gt 1 ]]; then
		jobstring="($jobcount jobs) "
	fi
	virtualenv=$( [ ! -z $VIRTUAL_ENV ] && basename $VIRTUAL_ENV )
	local zero='%([BSUbfksu]|([FB]|){*})'
	local vcs_width=${#${(S%%)vcs_info_msg_0_//$~zero/}}

        line0='%{$fg[red]%}%(?..✘ %{$fg_bold[black]%}$? $( last_status=$? && [[ $last_status -gt 128 ]] && echo "received signal $(builtin kill -l $(( $last_status - 128 )))" )
)%{$reset_color%}%'
	line1=' %{$fg[green]%}%d%{$reset_color%} ${vcs_info_msg_0_}'
	line2='
%B%(!.%{$fg[red]%}.%{$fg[white]%}$virtualenv%{$fg[white]%})%(2L.$(repeat $(( $SHLVL - 1 )) printf ">").)>%b%{$reset_color%} '
	rline1="%{$fg_bold[white]%}$jobstring%{$reset_color%}%{$fg_bold[$usercolor]%}%n%{$fg_bold[black]%}@%{$fg_bold[$hostcolor]%}%m%{$reset_color%}"

	line1_width=$(prompt_width $line1)
	rline1_width=$(prompt_width $rline1)
        filler_width=$(( 1 + COLUMNS - ( line1_width + rline1_width + vcs_width ) ))
        if [[ $filler_width -lt 0 ]]; then
            filler_width=0
        fi
	filler="%{$fg[white]%}${(l:$filler_width:: :)}%{$reset_color%}"
        PROMPT="${line0}${line1}${filler}${rline1}${line2}"
}
add-zsh-hook precmd vcs_info_update

# not sure yet
zstyle ':vcs_info:git*:*' get-revision true
# for hash: add %12.12i
zstyle ':vcs_info:git:*' formats "%{$fg[black]%}∓ %{$fg[blue]%}%b%{$reset_color%} %m%c%u%{$reset_color%} "
zstyle ':vcs_info:git:*' actionformats '∓ %b|%a'
zstyle ':vcs_info:hg:*' formats "%{$fg[black]%}☿ %{$fg[blue]%}%b%{$reset_color%}"
zstyle ':vcs_info:hg:*' actionformats "☿ %b|%a"

zstyle ':vcs_info:git*+set-message:*' hooks git-st # git-stash
# Show count of stashed changes
function +vi-git-stash() {
    local -a stashes

    if [[ -s ${hook_com[base]}/.git/refs/stash ]] ; then
        stashes=$(git stash list 2>/dev/null | wc -l)
        hook_com[misc]+=" (${stashes} stashed)"
    fi
}
# Show remote ref name and number of commits ahead-of or behind
function +vi-git-st() {
    local ahead behind remote
    local -a gitstatus

    # Are we on a remote-tracking branch?
    remote=${$(git rev-parse --verify ${hook_com[branch]}@{upstream} \
        --symbolic-full-name 2>/dev/null)/refs\/remotes\/}

    if [[ -n ${remote} ]] ; then
        # for git prior to 1.7
        # ahead=$(git rev-list origin/${hook_com[branch]}..HEAD | wc -l)
        ahead=$(git rev-list ${hook_com[branch]}@{upstream}..HEAD 2>/dev/null | wc -l)
        (( $ahead )) && gitstatus+=( "${c3}+${ahead}${c2}" )

        # for git prior to 1.7
        # behind=$(git rev-list HEAD..origin/${hook_com[branch]} | wc -l)
        behind=$(git rev-list HEAD..${hook_com[branch]}@{upstream} 2>/dev/null | wc -l)
        (( $behind )) && gitstatus+=( "${c4}-${behind}${c2}" )

        hook_com[branch]="${hook_com[branch]} [${remote} ${(j:/:)gitstatus}]"
    fi
}
