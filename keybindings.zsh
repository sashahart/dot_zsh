# defaults for some combinations used below
typeset -A key
key[Shift+Tab]='\e[Z'
key[Ctrl+Up]='\e[1;5A'
key[Ctrl+Down]='\e[1;5B'
key[Backspace]='^?'

# Keep this simple - just one file per host. Run zkbd manually to generate these.
# There is no very good automatic way of keeping track of the terminal, etc.
# This is at least cleaner/more modular than putting escapes like \e[3~
# directly in the bindkey statements.
term_prefix=`echo $TERM | cut -d '-' -f 1`
kbd_file="$HOME/.zkbd/${HOST}-${term_prefix}"
if [[ -e $kbd_file ]]; then
	source $kbd_file
# If no host file, just fall back to terminfo.
# from http://zshwiki.org/home/zle/bindkeys
else
	echo "Could not find $kbd_file. Defaulting to terminfo keys."
	key[Home]=${terminfo[khome]}
	key[End]=${terminfo[kend]}
	key[Insert]=${terminfo[kich1]}
	key[Delete]=${terminfo[kdch1]}
	key[Up]=${terminfo[kcuu1]}
	key[Down]=${terminfo[kcud1]}
	key[Left]=${terminfo[kcub1]}
	key[Right]=${terminfo[kcuf1]}
	key[PageUp]=${terminfo[kpp]}
	key[PageDown]=${terminfo[knp]}

	# Make sure terminal is in application mode when zle is active, so these
	# values from $terminfo will be valid.
	# This may not be necessary in linux console, which doesn't do smkx/rmkx
	# anyway.
	if [ "$TERM" != "linux" ] && [ "$TERM" != "dumb" ] ; then
		function zle-line-init () {
			# set cursor keys to 'application' mode
			echoti smkx
		}
		function zle-line-finish () {
			# reset cursor keys to normal or 'cursor' mode
			echoti rmkx
		}
		zle -N zle-line-init
		zle -N zle-line-finish  
	fi
fi


# vi keys (just as a baseline)
bindkey -v

if [ "$TERM" != "dumb" ]; then
	# vim-like missing stuff
	bindkey "$key[Home]"	beginning-of-line
	bindkey "$key[End]"		end-of-line
	bindkey "$key[Delete]"	delete-char
	bindkey "$key[Insert]"	quoted-insert

	# Shift-tab as inverse of tab completion
	bindkey "$key[Shift+Tab]" reverse-menu-complete
	# backspace works in any mode
	# this also allows it to be used to delete characters in interactive mode
	# without dismissing the mode
	bindkey -M vicmd "$key[Backspace]" backward-delete-char
	bindkey -M viins "$key[Backspace]" backward-delete-char
	# Ctrl-Up/Ctrl-Down do a prefix-based search, start typing and then hit one of
	# these to start cycling through MATCHING history.
	bindkey "$key[Ctrl+Up]" history-beginning-search-backward
	bindkey "$key[Ctrl+Down]" history-beginning-search-forward

	# In menu selection, hitting enter on an option just runs it, to avoid the
	# second enter hit. If you are building a list, just use space!
	bindkey -M menuselect '^M' .accept-line

	# paging in menu selection
	bindkey -M menuselect '^F' forward-word
	bindkey -M menuselect '^B' backward-word
	bindkey -M menuselect "$key[PageUp]" backward-word
	bindkey -M menuselect "$key[PageDown]" forward-word
fi

## Ctrl-X Ctrl-E to edit the current command line in editor
#autoload -Uz edit-command-line 
#zle -N edit-command-line 
#bindkey '^x^e' edit-command-line

# Alt-L completes line and pipes to less.
insert_less     () { zle end-of-line; zle -U "| less"; }
zle -N insert-less insert_less
bindkey "\el" insert-less

## :s in command mode does a search and replace
## this is not as good as the real vim one, but workable
autoload -U replace-string
vi-replace-string() {
  zle -K viins
  replace-string
}
zle -N vi-replace-string
bindkey -M vicmd :s vi-replace-string

## force Ctrl-U to work in vi cmd mode?
## still doesn't work.
#bindkey -a \eU    kill-whole-line
#bindkey -M vicmd "\eU" kill-whole-line
#bindkey -M viins "\eU" kill-whole-line
